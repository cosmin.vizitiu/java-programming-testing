package test.common.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "counties")
public class County {
	@Id
	private Integer id_county;

	@Column(length = 40, nullable = false, unique = false)
	private String county_name;

	@Column(length = 5, nullable = false)
	private String county_code;

	public County() {

	}

	public Integer getId_county() {
		return id_county;
	}

	public void setId_county(Integer id_county) {
		this.id_county = id_county;
	}

	public String getCounty_name() {
		return county_name;
	}

	public void setCounty_name(String county_name) {
		this.county_name = county_name;
	}

	public String getCounty_code() {
		return county_code;
	}

	public void setCounty_code(String county_code) {
		this.county_code = county_code;
	}
	
	public County(Integer id_county, String county_name, String county_code) {
		this.id_county = id_county;
		this.county_name = county_name;
		this.county_code = county_code;
	}

	@Override
	public String toString() {
		return "County [id_county=" + id_county + ", county_name=" + county_name + ", county_code=" + county_code + "]";
	}
	
}
