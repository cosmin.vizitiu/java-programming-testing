package test.common.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "localities")
public class Locality {
	@Id
	private Integer id;

	@Column(length = 40, nullable = false, unique = false)
	private String locality_name;
	

	@Column(length = 5, nullable = false)
	private String county_code;

	public Locality() {

	}

	public Integer getId_locality() {
		return id;
	}

	public void setId_locality(Integer id) {
		this.id = id;
	}

	public String getLocality_name() {
		return locality_name;
	}

	public void setLocality_name(String locality_name) {
		this.locality_name = locality_name;
	}

	public String getCounty_code() {
		return county_code;
	}

	public void setCounty_code(String county_code) {
		this.county_code = county_code;
	}
	
	public Locality(Integer id, String locality_name, String county_code) {
		this.id = id;
		this.locality_name = locality_name;
		this.county_code = county_code;
	}

	@Override
	public String toString() {
		return "Locality [id=" + id + ", locality_name=" + locality_name + ", county_code=" + county_code + "]";
	}
}
