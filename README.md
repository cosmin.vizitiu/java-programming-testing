In TestWebParent/TestBackEnd/src/main/java/test/admin/MyUserDetailsService.java we store the credentials for getting the token

Using the POST request to http://localhost:8080/authenticate and sending the credentials as JSON format we obtain the token

Using the token we can authorize the acces for the following:

- http://localhost:8080/counties - list of all the counties
- http://localhost:8080/localities - list of all the localities
- http://localhost:8080/users - list of all the users
- http://localhost:8080/createUser - create new user
- http://localhost:8080/user/id - get a specific user by id
- http://localhost:8080/updateUser - update existing user
- http://localhost:8080/deleteUser - delete user
