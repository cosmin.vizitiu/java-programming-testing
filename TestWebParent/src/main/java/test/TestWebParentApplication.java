package test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestWebParentApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestWebParentApplication.class, args);
	}

}
