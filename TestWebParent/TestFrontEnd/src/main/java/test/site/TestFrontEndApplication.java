package test.site;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestFrontEndApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestFrontEndApplication.class, args);
	}

}
