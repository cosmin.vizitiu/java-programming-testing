package test.admin.user;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.Rollback;

import test.admin.repository.UserRepository;
import test.common.entity.County;
import test.common.entity.Locality;
import test.common.entity.User;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
//@Rollback(false)
public class UserRepositoryTests {

	@Autowired
	private UserRepository repo;
	
	@Autowired
	TestEntityManager entityManager;
	@Test
	public void testCreateFirstUser() {
		User user = new User();
		Locality locality = entityManager.find(Locality.class, 5);
		County county = entityManager.find(County.class, 7);
		
		user.setName("Cosmin");
		user.setEmail("cosmin.vizitiu1997@yahoo.com");
		user.setLocality(locality);
		user.setCounty(county);
		User savedUser = repo.save(user);
		
		assertThat(savedUser.getId()).isGreaterThan(0);
	}
}
