package test.admin.user;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import test.admin.repository.LocalityRepository;
import test.common.entity.Locality;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
//@Rollback(false)
public class LocalityRepositoryTests {

	@Autowired
	private LocalityRepository repo;
	
	@Test
	public void testCreateFirstLocality() {
		Locality locality = new Locality(999,"Onesti","BC");
		Locality savedLocality = repo.save(locality);
		assertThat(savedLocality.getId_locality()).isGreaterThan(0);
	}
}
