package test.admin.user;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import test.admin.repository.CountyRepository;
import test.common.entity.County;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
//@Rollback(false)
public class CountyRepositoryTests {

	@Autowired
	private CountyRepository repo;
	
	@Test
	public void testCreateFirstCounty() {
		County county = new County(999,"Bacau","BC");
		County savedCounty = repo.save(county);
		assertThat(savedCounty.getId_county()).isGreaterThan(0);
	}
}
