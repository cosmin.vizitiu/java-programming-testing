--  Use testdb;
-- SELECT * FROM localities;
-- SELECT * FROM counties;
-- SELECT * FROM users_data;
-- DELETE FROM localities WHERE locality_name = 'Onesti'
-- DELETE FROM counties WHERE county_code = 'BC'


CREATE TABLE IF NOT EXISTS localities (
            id INT NOT NULL AUTO_INCREMENT,
            locality_name VARCHAR(40) NOT NULL,
            county_code VARCHAR(5) NOT NULL,
            PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS counties (
            id_county INT NOT NULL AUTO_INCREMENT,
            county_name VARCHAR(40) NOT NULL,
            county_code VARCHAR(5) NOT NULL,
            PRIMARY KEY (id_county)
);

CREATE TABLE IF NOT EXISTS users_data (
			id INT NOT NULL AUTO_INCREMENT,
			name VARCHAR(40) NOT NULL,
			email VARCHAR(40) NOT NULL UNIQUE,
			fk_locality_id INT,
			fk_county_id INT,
			FOREIGN KEY (fk_locality_id) REFERENCES localities(id),
			FOREIGN KEY (fk_county_id) REFERENCES counties(id_county),
			PRIMARY KEY(id)		
);

/* INSERT INTO users_data (
	name,
	email,
	fk_locality_id,
	fk_county_id
)
VALUES (
	'Cosmin',
	'cosmin.vizitiu1996@gmail.com',
	3,
	7
); */

-- SET GLOBAL local_infile=1;

/* LOAD DATA INFILE '/var/lib/mysql-files/test-java-fullstack-locality.csv'
INTO TABLE localities 
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS; */

/*LOAD DATA INFILE '/var/lib/mysql-files/test-java-fullstack-county.csv'
INTO TABLE counties 
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS; */

-- Show global variables like 'local_infile';

-- set global local_infile=on;
-- local_infile    = 1
-- SHOW VARIABLES LIKE "secure_file_priv";

-- select @@global.secure_file_priv;