package test.admin.controller;

import test.admin.repository.*;
import test.common.entity.User;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

	@Autowired
	UserRepository userRepository;
	
	public boolean isEmailUnique(String email) {
		User userByEmail = userRepository.getUserByEmail(email);
		if(userByEmail == null) {
			return true;
		}
		else {
			return false;
		}
	}
	
	@PostMapping("/createUser")
	private List<User> createUser(@RequestBody User user) {
		if(isEmailUnique(user.getEmail()) == true) {
			userRepository.save(user);
		} else {
			System.out.println("Email is not unique!");
			return getUsers();
		}
		//return user.getId();
		return getUsers();
	}
	
	@GetMapping("/users")
	private List<User> getUsers() {
		return (List<User>) userRepository.findAll();
	}
	
	@GetMapping("/user/{userId}")  
	private Optional<User> getUser(@PathVariable("userId") int userId)   
	{  
	return userRepository.findById(userId);
	}  
	
	@PutMapping("/updateUser")
	private List<User> updateUser(@RequestBody User user) {
		userRepository.save(user);
		return getUsers();
	}
	
	@DeleteMapping("/deleteUser/{id}")
	private List<User> deleteUser(@PathVariable("id") int id) {
		userRepository.deleteById(id);
		return getUsers();
	}
}
