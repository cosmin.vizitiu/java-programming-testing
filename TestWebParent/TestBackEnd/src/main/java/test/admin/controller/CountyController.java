package test.admin.controller;

import test.admin.repository.*;
import test.common.entity.County;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CountyController {
	
	@Autowired
	CountyRepository countyRepository;

	@GetMapping("/counties")
	public List<County> getCounties() {
		return (List<County>) countyRepository.findAll();
	}
}
