package test.admin.controller;

import test.admin.repository.*;
import test.common.entity.Locality;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LocalityController {

	@Autowired
	LocalityRepository localityRepository;
	
	@GetMapping("/localities")
	public List<Locality> getLocalities() {
		return (List<Locality>) localityRepository.findAll();
	}
	
}
