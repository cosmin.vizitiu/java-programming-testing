package test.admin.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import test.common.entity.County;

@Repository
public interface CountyRepository extends CrudRepository<County, Integer>{

}
