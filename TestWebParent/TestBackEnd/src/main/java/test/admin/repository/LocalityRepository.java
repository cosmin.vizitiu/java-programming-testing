package test.admin.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import test.common.entity.Locality;

@Repository
public interface LocalityRepository extends CrudRepository<Locality, Integer>{

}
